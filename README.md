# altium-intlib-self-build

## 0. 介绍

本自用集成库发展自 [嘉立创SMT样品贴片中可贴片元器件列表_基础库](https://gitee.com/JLC_SMT/JLCSMT_LIB)，适用于Altium，元器件大部分有3D模型。



## 1. 内容

### 1.1 元器件库

包含以下元器件库，每个库对应一个同名文件夹，各库内容如下。

|     库名称     |       元器件内容       |
| :------------: | :--------------------: |
| Common Headers |         接插件         |
|   JLC-intlib   | 嘉立创集成库（有扩展） |
|     Module     |      贴片PCB模块       |

以上库中包含的元器件请参阅 [wikis](https://gitee.com/beamworld/altium-intlib-self-build/wikis)。

### 1.2 丝印字体

在文件夹`/Font/`中有用于丝印层（也可用于其他层）的字体，每个库对应一个同名文件夹，各库内容如下。

|   字体名称    |       用途       |                             说明                             |
| :-----------: | :--------------: | :----------------------------------------------------------: |
| PCB_LOGO_SILK | PCB 常用图标丝印 | 来自B站@[chaoranhappy](https://space.bilibili.com/505503163)，[点击这里跳转](https://www.bilibili.com/read/cv14644033/) |

### 1.3 Altium 脚本

在文件夹`/Script/`中有Altium用脚本，每个脚本对应一个同名文件夹，各脚本内容如下。

|     字体名称     |      功能      |                 说明                 |
| :--------------: | :------------: | :----------------------------------: |
| PCB Logo Creator | 位图转换为图形 | 可以把各种图标转换为用线条组成的图形 |



## 2.内容预览

@todo



## 3. 使用方法

### 3.1 获取

使用 git 克隆或下载zip。

1.   git 克隆

     使用如下指令克隆到本地：

     ```bash
     git clone https://gitee.com/beamworld/altium-intlib-self-build
     ```

2.   下载zip

     点击下载 [altium-intlib-self-build](https://gitee.com/beamworld/altium-intlib-self-build/repository/archive/master.zip)

### 3.2 编译

以 `Common Headers` 库为例：

1.   使用 `altium` 打开库文件 `Common Headers.LibPkg`；
2.   点击软件左上角 `Project` 选项，点击 `Compile Integrated Library Common Headers.LibPkg`；
3.   编译成功将在 `Common Headers` 库根目录下的 `Project Outputs for Common Headers` 文件夹下会生成集成库 `Common Headers.IntLib`，并且该集成库已经自动安装到 Altium 软件；

### 3.3 字体使用

点击后缀为`.ttf`的文件直接安装到系统，重启Altium后选择该字体即可。



## 4. 许愿池

-   如果没有找到你需要的元器件，或者你对某个元器件不满意，可以通过 [issues](https://gitee.com/beamworld/altium-intlib-self-build/issues) 告诉我，你会获得随缘处理~~

-   如果你要好的封装要分享，你可以首先 `Fork` 这个仓库，做出更改后发起合并请求。
